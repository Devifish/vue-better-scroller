import Scroll from "./Scroll";

function install(Vue) {
  if (!Vue) return;
  
  //全局导入
  Vue.component("Scroll", Scroll);
}

// 判断是否是直接引入文件
if (typeof window !== 'undefined' && window.Vue) {
  install(window.Vue);
}

export default {
  install,
  Scroll
}