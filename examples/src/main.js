import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Scroll from "./../../src/index";

Vue.config.productionTip = false

Vue.use(Scroll);

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
