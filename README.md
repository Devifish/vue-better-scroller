# vue-better-scroller
> 一款基于 [**better-scroll.js**](https://github.com/ustbhuangyi/better-scroll) （已内置依赖无需导包）<br/>
> 为 vue.js 提供 scroll 滚动，上拉加载， 下拉刷新<br/>
> 使用 es6， vue-cli 3 构建

## 特点
- 得益于 better-scroll 使用 css3 transform3d 硬件加速实现滚动<br/>
  即使千条数据也不会有卡顿掉帧
- 使用 css3 calc 实现内部高度，避免使用原生better-scroll的不愉快
- 尽量减少用户配置可渐进式添加功能

## 安装
NPM安装
``` bash
$ npm install vue-better-scroller
```
也可手动下载Common JS 及 UMD JS手动导入
编译好的js文件 放于 dist 文件夹

## 使用
在 `main.js` 文件中引入插件并注册
``` bash
# main.js
import Scroll from "vue-better-scroller";
Vue.use(Scroll)
```
```html
<template>
  <scroll>
    <li></li>
    <li></li>
    <li></li>
    <li></li>
  </scroll>
</template>
```

## 文档
组件 attr 参数文档
<table style="text-align: center">
  <thead>
    <tr>
      <td>名称</td>
      <td>功能</td>
      <td>默认值</td>
      <td>可选值</td>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>data</td>
      <td>用于监听数据变化停止上拉下拉状态</td>
      <td>空</td>
      <td>object / array</td>
    </tr>
    <tr>
      <td>scrollbar</td>
      <td>是否显示滚动条</td>
      <td>false</td>
      <td>true / false</td>
    </tr>
    <tr>
      <td>direction（开发中）</td>
      <td>滚动方向</td>
      <td>vertical</td>
      <td>vertical / horizontal</td>
    </tr>
    <tr>
      <td>bounce</td>
      <td>各方向回弹效果开关</td>
      <td>{ top: true, bottom: true, left: true, right: true }</td>
      <td>修改相应参数即可</td>
    </tr>
    <tr>
      <td>options</td>
      <td>可选功能（如开启关闭上拉加载下拉刷新）</td>
      <td>{ pullup: false, pulldown: false, click: true, probeType: 0 }</td>
      <td>修改相应参数即可</td>
    </tr>
  </tbody>
</table>

## 例子
- 单独使用滚动（添加滚动回弹效果及滚动条）
  ```html
    <scroll :scrollbar="true">
      <li></li>
      <li></li>
      <li></li>
      <li></li>
    </scroll>
  ```

- 添加上拉刷新及下拉加载
  ```html
    <scroll :options="{ pullup: true, pulldown: true }" :ref="scroll">
      <li></li>
      <li></li>
      <li></li>
      <li></li>
    </scroll>
  ```
  注意： 在上拉加载结束后需要调用
  ```js
    this.$refs.scroll.pullupEnd();
  ```
  关闭上拉加载功能

## 效果
下面截图于本人公司项目DEMO实现 因此无法提供DEMO代码<br/>

<img src="https://gitee.com/Devifish/vue-better-scroller/raw/master/screenshot/screenshot_1.gif" width="300" height="540"/>
<img src="https://gitee.com/Devifish/vue-better-scroller/raw/master/screenshot/screenshot_2.gif" width="300" height="540"/>
<img src="https://gitee.com/Devifish/vue-better-scroller/raw/master/screenshot/screenshot_3.gif" width="300" height="540"/>
